import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-tests',
  templateUrl: './tests.component.html',
  styleUrls: ['./tests.component.css']
})
export class TestsComponent implements OnInit {

  constructor( private http: HttpClient) { }

  test:any
  xyz:any
  golang:any

  ngOnInit(): void {
    this.test='Test zmiennej';

    this.http.get('https://jsonplaceholder.typicode.com/todos/1').subscribe(res=>{
      this.xyz=res;
    });
    this.http.get('http://localhost:8080/hello').subscribe(res=>{
      this.golang=res;
    })
  }

}
